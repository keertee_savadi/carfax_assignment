//
//  ViewController.swift
//  Carfax
//
//  Created by Keertee on 7/10/19.
//  Copyright © 2019 Keertee. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import SVProgressHUD

class CarDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var carListTableView: UITableView!
    
    let carModel = CarModel()
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        addPullToRefreshView()
        populateCarDetails()
        
        // Do any additional setup after loading the view.
    }
    
    func addPullToRefreshView()
    {
        if #available(iOS 10.0, *)
        {
            carListTableView.refreshControl = refreshControl
        }
        else
        {
            carListTableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refresh( _:)), for: UIControl.Event.valueChanged)
    }
    
    @objc func refresh(_ sender: Any)
    {
        populateCarDetails()
    }
    
    func populateCarDetails()
    {
        let carVM = CarViewModel()
        SVProgressHUD.show()
        carVM.getCarDetailsFromServer { (responseObject) in
            SVProgressHUD.dismiss()
            self.refreshControl.endRefreshing()
            self.carModel.parseResponseObject(responseObject)
            self.carListTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 400
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (carModel.cars.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:CarViewCell = tableView.dequeueReusableCell(withIdentifier: "CarViewCell") as! CarViewCell
        if(carModel.cars.count > 0 && indexPath.row < carModel.cars.count)
        {
            var firstLabelText:String = ""
            var secondLabelText = ""
            
            if let make = carModel.cars[indexPath.row].make
            {
                firstLabelText = make + " "
            }
            
            if let year = carModel.cars[indexPath.row].year?.stringValue
            {
                firstLabelText = firstLabelText + year + " "
            }
            
            if let model = carModel.cars[indexPath.row].model
            {
                firstLabelText = firstLabelText + model + " "
            }
            
            if let trim = carModel.cars[indexPath.row].trim
            {
                firstLabelText = firstLabelText + trim + " "
            }
            
            if let price = carModel.cars[indexPath.row].price?.stringValue
            {
                secondLabelText = secondLabelText + "$" + price + " | "
            }
            
            if let mileage = carModel.cars[indexPath.row].mileage?.stringValue
            {
                secondLabelText = secondLabelText + mileage + " Mi | "
            }
            
            if let city = carModel.cars[indexPath.row].city
            {
                secondLabelText = secondLabelText + " | " + city
            }
            
            if let state = carModel.cars[indexPath.row].state
            {
                secondLabelText = secondLabelText + "," + state
            }
    
            cell.carBasicDetailsLabel.text = firstLabelText
            cell.carPriceDetailsLabel.text = secondLabelText
            cell.carDealerContactButton.setTitle(carModel.cars[indexPath.row].contact, for: .normal)
            cell.carDealerContactButton.tag = indexPath.row
            cell.carDealerContactButton.addTarget(self, action: #selector(callNumber(sender:)), for: .touchUpInside)
            
            if(carModel.cars[indexPath.row].images.count > 0)
            {
                cell.carImageView.setImageForUrl(carModel.cars[indexPath.row].images[0])
            }
        }
       
        return cell
    }

    @objc private func callNumber(sender:UIButton)
    {
        if let contactString = carModel.cars[sender.tag].contact
        {
            if let phoneCallURL: NSURL = URL(string: "TEL://\(contactString))")! as NSURL
            {
                
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(phoneCallURL as URL))
                {
                    if #available(iOS 10.0, *)
                    {
                        application.open(phoneCallURL as URL, options: [:], completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                        application.openURL(phoneCallURL as URL)
                        
                    }
                }
                else
                {
                    let alert = UIAlertController(title: "Oops!", message: "Cannot call in simulator", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}

extension UIImageView
{
    func setImageForUrl(_ imageUrl:String)
    {
        SDWebImageManager.shared.loadImage(
            with: URL(string:imageUrl),
            options: .highPriority,
            progress: nil) { (image, data, error, cacheType, isFinished, imageUrl) in
                self.image = image
        }
    }
}
