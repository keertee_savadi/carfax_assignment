//
//  CarModel.swift
//  Carfax
//
//  Created by Keertee on 7/10/19.
//  Copyright © 2019 Keertee. All rights reserved.
//

import UIKit

class CarModel: NSObject
{
    var carImageUrl:String?
    var year:NSNumber?
    var make:String?
    var model:String?
    var trim:String?
    var price:NSNumber?
    var mileage:NSNumber?
    var city:String?
    var state:String?
    var contact:String?
    var images = Array<String>()
    
    var cars = Array<CarModel>()
    
    func parseResponseObject(_ responseObject:Dictionary<String,AnyObject>)
    {
        if let carListings = responseObject["listings"] as? NSArray
        {
            for car in carListings
            {
                print(car)
                let cm = CarModel()
                
                let carDict = car as? Dictionary<String,AnyObject>
                
                if let trim = carDict?["trim"]
                {
                    cm.trim = (trim as! String)
                }
                
                if let year = carDict?["year"]
                {
                    cm.year = year as? Int as NSNumber?
                }
                
                if let make = carDict?["make"]
                {
                    cm.make = (make as! String)
                }
                
                if let model = carDict?["model"]
                {
                    cm.model = (model as! String)
                }

                if let price = carDict?["listPrice"]
                {
                    cm.price = price as? Int as NSNumber?
                }
                
                if let mileage = carDict?["mileage"]
                {
                    cm.mileage = mileage as? Int as NSNumber?
                }
                
                if let contact = carDict?["dealer"]?["phone"]
                {
                    cm.contact = (contact as! String)
                }
                
                if let images = carDict?["images"]?["firstPhoto"]
                {
                    let imageDict = images as? Dictionary<String,AnyObject>
                    
                    if let largeImage = imageDict!["large"]
                    {
                        cm.images.append(largeImage as! String)
                    }
                    if let smallImage = imageDict!["small"]
                    {
                        cm.images.append(smallImage as! String)
                    }
                    if let mediumImage = imageDict!["medium"]
                    {
                        cm.images.append(mediumImage as! String)
                    }
                }
                
                if let city = carDict?["dealer"]?["city"]
                {
                    cm.city = city as? String
                }
                
                if let state = carDict?["dealer"]?["state"]
                {
                    cm.state = state as? String
                }
                
                cars.append(cm)
            }
        }
        
    }
    
    
    
}
