//
//  CarViewCell.swift
//  Carfax
//
//  Created by Keertee on 7/10/19.
//  Copyright © 2019 Keertee. All rights reserved.
//

import UIKit

class CarViewCell: UITableViewCell {

    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var carBasicDetailsLabel: UILabel!
    @IBOutlet weak var carPriceDetailsLabel: UILabel!
    @IBOutlet weak var carDealerContactButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
