//
//  CarViewModel.swift
//  Carfax
//
//  Created by Keertee on 7/10/19.
//  Copyright © 2019 Keertee. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

typealias CompletionHandler = (_ responseObject:Dictionary<String,AnyObject>) -> Void

class CarViewModel: NSObject
{
    let carDetailsURL = "https://carfax-for-consumers.firebaseio.com/assignment.json"
    
    func getCarDetailsFromServer(completionHandler: @escaping CompletionHandler)
    {
        Alamofire.request(carDetailsURL).responseJSON { response in
            if let json = response.result.value as? Dictionary<String, AnyObject>
            {
                completionHandler(json)
            }
        }
    }


}

